# PhoneBookApp-in-java



public class PhoneBookApp {
 
 public static void main(String[] args){
  PhoneBook myPB = new PhoneBook();
  
  myPB.addFriend(new Friend("Caryn", "215-555-1234"));
  myPB.addFriend(new Friend("Jane",  "215-555-2222"));
  myPB.addFriend(new Friend("Robin", "215-555-2345"));
  myPB.addFriend(new Friend("Sam", "215-555-1867"));
  myPB.addFriend(new Friend("Elliot", "215-555-2222"));
  myPB.addFriend(new Friend("Deborah", "215-555-6543"));
  myPB.addFriend(new Friend("Sally", "215-555-8133"));
  
  System.out.println("pages:" + myPB.getNumPages() + ", friends: " + myPB.getNumFriends());
  System.out.println(myPB.toString());
  System.out.println("Sam's number: " + myPB.getNumber("Sam"));
  System.out.println("last friend: " + myPB.getFriend(myPB.getNumFriends() - 1));
 }
}  
  
